#include <stdio.h>
#include <linux/futex.h>
#include <sys/time.h>
#include <sys/syscall.h>
#include <unistd.h>
#include "mutex.h"
#include "spinlock.h"

static int futex(int *uaddr, int futex_op, int val ){
    return syscall(SYS_futex, uaddr, futex_op, val, NULL, NULL, 0);
}

int thread_mutex_init( thread_mutex_t *lock ){
    assert(lock);
    atomic_store( lock, 0 );
    return 0;
}

int thread_mutex_lock( thread_mutex_t *lock ){
    // if( !lock )
    //     return EINVAL ;
    // if( !atomic_cmp_exchg( lock, UNLOCKED, LOCKED) )
    //     return LOCKED ;
    // return UNLOCKED;

    if( !lock )
        return EINVAL ;
    int c = atomic_cmp_exchg( lock, UNLOCKED, LOCKED);
    if( c == 0 ) {
        do {
            if( c == 2 || atomic_cmp_exchg( lock, LOCKED, WAITING) == 2 ) {
                futex( lock, FUTEX_WAIT, WAITING );
            }
        } while( (c = atomic_cmp_exchg( lock, UNLOCKED, WAITING) ) != 0);
    }
    return 0;
    return LOCKED;
}

int thread_mutex_trylock( thread_mutex_t *lock ){
    if( !lock )
        return EINVAL ;
    if( !atomic_cmp_exchg( lock, UNLOCKED, LOCKED) )
        return LOCKED ;
    return UNLOCKED;
}

int thread_mutex_unlock( thread_mutex_t *lock ) {
    if( !lock )
        return EINVAL ;
    atomic_cmp_exchg( lock, LOCKED, UNLOCKED);
    futex(lock, FUTEX_WAKE, 1);
    return UNLOCKED;
}