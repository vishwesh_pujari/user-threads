#define _GNU_SOURCE
#include <unistd.h>
#include "thread.h"
#include <stdlib.h> // for malloc()
#include <errno.h>
#include <sched.h> // for clone()
#include <signal.h> // for SIGCHLD
#include <stdio.h>
#include <setjmp.h>
#include <sys/time.h>
#include <stdatomic.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <ucontext.h>

enum state {
    WAITING,
    RUNNABLE,
    ENDED
};

typedef struct thread {
    // user_tid is index of 'arr' where thread is stored
    thread_t user_tid;

    // context to be used by makecontext(), swapcontext()
    ucontext_t context;

    // function which is to be executed by the thread
    void *(*funcptr)(void *);

    // arguments to be passed to the function
    void *arg;

    // return value of the function
    void *retval;

    // If equal to 1 means someone already call join on this thread
    char waiting_to_join ;

    // buffer for storing setjmp(), longjmp() info
    jmp_buf setjump_buf;

    char state;

    // which thread has called join on this thread
    int whoiswaiting;

    // the set for pending signals
    sigset_t set;
}thread;

// Maximum number of simultaneous threads
#define NTHREAD 100
#define STACK_SIZE (4096 * 10)
#define SECONDS 0
#define MICROSECONDS (suseconds_t) 100

struct itimerval timer = {{SECONDS, MICROSECONDS}, {SECONDS, MICROSECONDS}};

thread* arr[NTHREAD] = {0};
int allocated_count = 0;
int current = 0; // index of user thread which is currently running

void interrupt_enable() {
    /* Enable timer (counts down in real time) */
    timer.it_value.tv_sec = SECONDS;
    timer.it_value.tv_usec = MICROSECONDS;
	timer.it_interval.tv_sec = SECONDS;
	timer.it_interval.tv_usec = MICROSECONDS;
    setitimer(ITIMER_VIRTUAL, &timer, 0);
}

void interrupt_disable() {
    /* Disable timer */
    timer.it_value.tv_sec = 0;
    timer.it_value.tv_usec = 0;
	timer.it_interval.tv_sec = 0;
   	timer.it_interval.tv_usec = 0;
    setitimer(ITIMER_VIRTUAL, &timer, 0);
}

void scheduler() {
    interrupt_disable();
    if (allocated_count == 0) {
        interrupt_enable();
        return;
    }
    int i;
    for (i = (current + 1) % NTHREAD; !arr[i] || arr[i]->state != RUNNABLE; i = (i + 1) % NTHREAD);
    int temp = current;
    current = i;
    interrupt_enable();
    //printf("temp = %d, i = %d\n", temp, i);
    swapcontext(&arr[temp]->context, &arr[i]->context);
    return;
}

void thread_begin(int i) {
    thread* tptr = arr[i];

    int ret = setjmp(tptr->setjump_buf);
    if (ret == 0) {

        // call the function which is to be executed on the thread
        // store the return value
        tptr->retval = tptr->funcptr(tptr->arg);
    }

    if( ret == 0 )
        interrupt_disable();

    if (tptr->whoiswaiting != -1)
        arr[tptr->whoiswaiting]->state = RUNNABLE;
    else {
        tptr->state = ENDED;
        allocated_count--;
    }
    interrupt_enable();
    scheduler();
    return;
}

void thread_exit(void *retval) {
    interrupt_disable();
    arr[current]->retval = retval;

    if (current == 0) { // thread_exit() has been called from main()
        arr[0]->state = ENDED;
        allocated_count--;
        interrupt_enable();
        scheduler();
    } else {
        longjmp(arr[current]->setjump_buf, 1);
    }
}

void sighandler(int signum) {
    if (signum == SIGVTALRM) {
        scheduler();

        // raise all the pending signals
        interrupt_disable();
        for (int i = 1; i < NSIG; i++)
            if (sigismember(&arr[current]->set, i))
                raise(i);
        sigemptyset(&arr[current]->set);
        interrupt_enable();
    }
    
    return;
}

int thread_create(thread_t *thread_id, thread_attr_t *attr, void *(*funcptr) (void *), void *arg) {
    interrupt_disable();
    if (allocated_count == NTHREAD) {
        interrupt_enable();
        return EAGAIN; // Resource(arr) temporarily unavailable
    }

    if (thread_id == NULL || funcptr == NULL) {
        interrupt_enable();
        return EINVAL;
    }
    // if the main thread is not stored in array
    if (arr[0] == NULL) {
        arr[0] = (thread*) calloc(1, sizeof(thread));
        if (!arr[0]) {
            interrupt_enable();
            return errno;
        }
        arr[0]->user_tid = 0;
        arr[0]->state = RUNNABLE;
        arr[0]->whoiswaiting = -1;
        arr[0]->waiting_to_join = 0;
        if (sigemptyset(&arr[0]->set) == -1) {
            interrupt_enable();
            return errno;
        }
        allocated_count++;
        signal(SIGVTALRM, sighandler);
    }

    thread *t = (thread*) malloc(sizeof(thread));
    if (!t) {
        interrupt_enable();
        return errno;
    }

    if (getcontext(&(t->context)) == -1) {
        free(t);
        interrupt_enable();
        return errno;
    }

    if( !attr )
        t->context.uc_stack.ss_size = STACK_SIZE;
    else
        t->context.uc_stack.ss_size = attr->stack_size ;
    
    if( attr && attr->stack )
        t->context.uc_stack.ss_sp = attr->stack ;
    else
        t->context.uc_stack.ss_sp = mmap(NULL, t->context.uc_stack.ss_size, PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK, -1, 0);

    if( attr && !(attr->stack) )
        attr->stack = t->context.uc_stack.ss_sp ;
    
    t->context.uc_link = NULL;
    if (!t->context.uc_stack.ss_sp) {
        free(t);
        interrupt_enable();
        return errno;
    }

    // find a NULL or has_ended entry in arr to store thread
    int i;
    for (i = 0; i < NTHREAD; i++)
        if (arr[i] == NULL || arr[i]->state == ENDED)
            break;
    
    // if arr[i] is not NULL, means that arr[i]->has_ended is true
    if (arr[i]) {
        // cleanup
        free(arr[i]->context.uc_stack.ss_sp);
        free(arr[i]);
        arr[i] = NULL;
    }
    
    arr[i] = t;
    allocated_count++;
    *thread_id = i;

    t->user_tid = i;
    t->funcptr = funcptr;
    t->arg = arg;
    t->retval = NULL;
    t->waiting_to_join = 0 ;
    t->state = RUNNABLE;
    t->whoiswaiting = -1;
    if (sigemptyset(&t->set) == -1) {
        interrupt_enable();
        return errno;
    }

    makecontext(&(t->context), thread_begin, 1, i);
    interrupt_enable();
    return 0;
}

int thread_join(thread_t thread_id, void **retval){
    interrupt_disable();

    // If thread_id is not is bound
    if( thread_id >= NTHREAD || thread_id < 0 ){
        interrupt_enable();
        return ESRCH ; // No thread with this id
    }
    /* If thread is already over or thread id is invalid
     * Or someone already call join on thread
     */
    if( !arr[thread_id] || arr[thread_id]->waiting_to_join == 1 || arr[thread_id]->state == ENDED) {
        interrupt_enable();
        return ESRCH ; 
    }

    // Set value for waiting_to_join
    arr[thread_id]->waiting_to_join = 1 ;
    arr[thread_id]->whoiswaiting = current;
    arr[current]->state = WAITING;
    interrupt_enable();
    scheduler();

    // Store return value
    if (retval)
        *retval = arr[thread_id]->retval ;
    
    interrupt_disable();
    // end the thread on which we were waiting
    arr[thread_id]->state = ENDED;
    allocated_count--;

    interrupt_enable();
    return 0 ;
}

int thread_kill(thread_t thread_id, int sig) {
    interrupt_disable();
    if ( thread_id < 0 )
        return ESRCH; // No thread with this id

    if ( !arr[thread_id] || arr[thread_id]->state == ENDED )
        return ESRCH;
    
    if ( sig == 0 )
        return EINVAL;
    
    if ( arr[0] == NULL ) // thread group leader not present
        return EINVAL;
    
    sigaddset(&arr[thread_id]->set, sig);
    interrupt_enable();
    return 0;
}

int thread_attr_init( thread_attr_t *attr ){
    if( attr == NULL )
        return EINVAL;
    attr->stack_size = STACK_SIZE ;
    attr->stack = NULL ;
    return 0;
}

int thread_attr_destroy( thread_attr_t *attr ){
    if( !attr )
        return EINVAL ;
    if( attr->stack )
        free( attr->stack );
    return 0 ;
}