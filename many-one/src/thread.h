typedef unsigned long int thread_t;

typedef struct thread_attr_t{
    int stack_size;  // Stack size
    void *stack;  // Stack base pointer
}thread_attr_t;

int thread_attr_init( thread_attr_t *attr );
int thread_attr_destroy( thread_attr_t *attr );

int thread_create(thread_t *thread_id, thread_attr_t *attr, void *(*funcptr) (void *), void *arg);
int thread_join(thread_t thread_id, void **retval);
void thread_exit(void *retval);
int thread_kill(thread_t thread, int sig);