gcc -Wall matrix.c ../src/thread.c ../src/spinlock.c -o matrix
gcc -Wall thread_join.c ../src/thread.c ../src/spinlock.c -o thread_join
gcc -Wall spin_test.c ../src/thread.c ../src/spinlock.c -o spin_test
gcc -Wall thread_create.c ../src/thread.c ../src/spinlock.c -o thread_create
gcc -Wall mutex_test.c ../src/thread.c ../src/spinlock.c ../src/mutex.c -o mutex_test
gcc -Wall thread_kill.c ../src/thread.c ../src/spinlock.c -o thread_kill

./matrix
if [ $? -eq 0 ]
then
	echo "Passed"
else
	echo "Failed"
fi

./thread_join
if [ $? -eq 0 ]
then
	echo "Passed"
else
	echo "Failed"
fi

./spin_test
if [ $? -eq 0 ]
then
	echo "Passed"
else
	echo "Failed"
fi

./thread_create
if [ $? -eq 0 ]
then
	echo "Passed"
else
	echo "Failed"
fi

./mutex_test
if [ $? -eq 0 ]
then
	echo "Passed"
else
	echo "Failed"
fi

./thread_kill

rm matrix thread_join spin_test thread_create mutex_test thread_kill
