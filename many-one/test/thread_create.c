#include <stdio.h>
#include "../src/thread.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/syscall.h>

int glo = 0;

void* func(void* arg) {
    return NULL;
}

int main(){
    thread_t tid;
    int ret;

    printf("Creating thread with Invalid tid \n");
    ret = thread_create( NULL, NULL, func, NULL);
    if( ret == 0 )
        return 1;

    printf("Creating thread with NULL function pointer \n");
    ret = thread_create( &tid, NULL, NULL, NULL);
    if( ret == 0 )
        return 1;

    printf("Creating thread with Valid Arguments \n");
    ret = thread_create( &tid, NULL, func, NULL);
    if( ret != 0 )
        return 1;

    printf("Creating thread with thread attribute ( only specifing stack size ) \n");
    thread_attr_t attr1 ;
    attr1.stack_size = 4096*64 ;
    attr1.stack = NULL ;
    ret = thread_create( &tid, &attr1, func, NULL);
    if( ret != 0 )
        return 1;

    printf("Creating thread with thread attribute \n");
    thread_attr_t attr2 ;
    attr2.stack_size = 4096*64 ;
    attr2.stack = mmap( NULL, attr2.stack_size, PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK, -1, 0);
    ret = thread_create( &tid, &attr2, func, NULL);
    if( ret != 0 )
        return 1;

    printf("Test Successful..\n");
    return 0 ;
}
