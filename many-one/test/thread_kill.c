#include"../src/thread.h"
#include<stdio.h>
#include<signal.h>
#include<unistd.h>

void sighand(int signum) {
    if (signum == SIGSEGV) {
        printf("signal occurred\n");
        printf("Test Successful \n");
    }
    return;
}
int run = 1;
void *f1(void *arg) {
    signal(SIGSEGV, sighand);
    while (run);
    return NULL;
}

int main() {
    thread_t tid;
    int ret = thread_create(&tid, NULL, f1, NULL);
    if (ret != 0) {
        printf("thread_create() failed\n");
        return ret;
    }
    for (int i = 0; i < 100000000; i++);
    printf("sending signal\n");
    ret = thread_kill(tid, SIGSEGV);
    // printf("ret = %d\n", ret);
    // printf("done\n");
    run = 0;
    thread_join(tid, NULL);
    return 0;
}