#include <stdio.h>
#include "../src/thread.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <errno.h>

int glo = 0;

void *square( void *arg ) {
    int *i = (int*)arg;
    sleep(1);
    int *val = (int*)malloc(sizeof(int));
    *val = (*i)*(*i) ;
    return val;
}

int main(){
    thread_t tid;
    int ret, i = 2;
    void *ret_val ;

    printf("-------------------------------------------------- \n");
    printf("\t\t Thread Join \n");
    printf("-------------------------------------------------- \n");

    printf("Calling thread_join with Invalid tid \n");
    ret = thread_create( &tid, NULL, square, &i );
    if( ret != 0 )
        return 1;
    ret = thread_join( -1, &ret_val);
    if( ret != ESRCH )
        return 1;

    printf("Calling thread_join with Valid Arguments \n");
    ret = thread_create( &tid, NULL, square, &i );
    if( ret != 0 )
        return 1;
    thread_join( tid, &ret_val);

    if( 4 == *((int*)ret_val) )
        printf("Test Successful \n");
    return 0 ;
}
