#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <errno.h>
#include "../src/thread.h"
#include "../src/spinlock.h"

int row1, col1, row2, col2, current_row = 0, **arr1, **arr2, **result;

thread_spinlock_t spin_row ;

void *multiply( void *arg ){
	int i, j, row;
    while(1){
        thread_spin_lock( &spin_row );
        if( current_row >= row1 ){
            thread_spin_unlock( &spin_row );
            thread_exit(NULL);
        } 
        row = current_row;
        current_row++;
        thread_spin_unlock(&spin_row);
        for (j = 0; j < col2; j++)
            for (i = 0; i < col1; i++)
                result[row][j] += arr1[row][i] * arr2[i][j];
    } 
    return NULL ;
} 


int main(int argc, char *argv[]){
    int i, j;
    thread_spin_init(&spin_row);

    row1 = 40;
    col1 = 40;
    row2 = 40;
    col2 = 40;
	
    if( col1 != row2 )
        return 0;	
		
    int no_row = row1;

    arr1 = (int**)malloc(sizeof(int*) * row1);
    for (i = 0; i < row1; i++)
        arr1[i] = (int*)malloc(sizeof(int) * col1);

    arr2 = (int**)malloc(sizeof(int*) * row2);
    for (i = 0; i < row2; i++)
        arr2[i] = (int *) malloc(sizeof(int) * col2);

    result = (int **) malloc(sizeof(int*) * row1);
    for (i = 0; i < row1; i++)
        result[i] = (int *) malloc(sizeof(int) * col2);

    for (i = 0; i < row1; i++)
        for (j = 0; j < col1; j++)
            arr1[i][j] = 1;

    for (i = 0; i < row2; i++)
        for (j = 0; j < col2; j++)
            arr2[i][j] = 2;
	
    thread_t *tid = (thread_t *) malloc(sizeof(thread_t) * no_row);
			
    current_row = 0;
	for (i = 0; i < no_row; i++)
        thread_create(&tid[i], NULL, multiply, NULL );
		
	for (i = 0; i < no_row; i++)
        thread_join(tid[i], NULL);

    for (i = 0; i < row1; i++) {
        for (j = 0; j < col2; j++)
            if( result[i][j] != 80 )
                return 1 ;
    }
    
	printf("Test Successful\n");
    return 0;
} 
