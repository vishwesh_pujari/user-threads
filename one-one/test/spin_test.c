#include <stdio.h>
#include "../src/thread.h"
#include "../src/spinlock.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/syscall.h>

thread_spinlock_t lock ;
int c = 0, c1 = 0, c2 = 0, c3 = 0, c4 = 0 ;
int flag = 1 ;

void *fun1( void *arg ) {
	while(flag) {
		thread_spin_lock(&lock);
		c++;
		thread_spin_unlock(&lock);
		c1++;
	}
	return NULL;
}

void *fun2( void *arg ) {
	while(flag) {
		thread_spin_lock(&lock);
		c++;
		thread_spin_unlock(&lock);
		c2++;
	}
	return NULL;
}

void *fun3( void *arg ) {
	while(flag) {
		thread_spin_lock(&lock);
		c++;
		thread_spin_unlock(&lock);
		c3++;
	}
	return NULL;
}

void *fun4( void *arg ){
	while(flag) {
		thread_spin_lock(&lock);
		c++;
		thread_spin_unlock(&lock);
		c4++;
	}
	return NULL;
}

int main(int argc, char **argv) {
	thread_t tid1, tid2, tid3, tid4 ;
    thread_spin_init(&lock);
	int ret;

    ret = thread_create( &tid1, NULL, fun1, NULL);
    if( ret != 0 )
        return 1;

    ret = thread_create( &tid2, NULL, fun2, NULL);
    if( ret != 0 )
        return 1;

    ret = thread_create( &tid3, NULL, fun3, NULL);
    if( ret != 0 )
        return 1;

    ret = thread_create( &tid4, NULL, fun4, NULL);
    if( ret != 0 )
        return 1;

    sleep(2);
	flag = 0 ;

	thread_join( tid1, NULL );
	thread_join( tid2, NULL );
	thread_join( tid3, NULL );
	thread_join( tid4, NULL );

	//printf("c1 %d, c2 %d, c3 %d, c4 %d, total %d, c %d \n", c1, c2, c3, c4, c1+c2+c3+c4, c );
    if( c1+c2+c3+c4 != c )
		return 1;
    
	printf("Successful..\n");
    return 0 ;
}