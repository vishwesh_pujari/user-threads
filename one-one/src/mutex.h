#include <stdatomic.h>
#include <errno.h>
#include <assert.h>

#define UNLOCKED 0
#define LOCKED 1
#define WAITING 2

typedef int thread_mutex_t;

int thread_mutex_init( thread_mutex_t *lock );
int thread_mutex_lock( thread_mutex_t *lock );
int thread_mutex_trylock( thread_mutex_t *lock );
int thread_mutex_unlock( thread_mutex_t *lock );