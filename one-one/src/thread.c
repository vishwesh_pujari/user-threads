#define _GNU_SOURCE
#include <unistd.h>
#include "thread.h"
#include <stdlib.h> // for malloc()
#include <errno.h>
#include <sched.h> // for clone()
#include <signal.h> // for SIGCHLD
#include <stdio.h>
#include <sys/wait.h>
#include <setjmp.h>
#include <linux/futex.h>
#include <sys/time.h>
#include <stdatomic.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include "spinlock.h"

enum state {
    RUNNING,
    ENDED
};

thread_spinlock_t glock ;

static int futex(int *uaddr, int futex_op, int val ){
    return syscall(SYS_futex, uaddr, futex_op, val, NULL, NULL, 0);
}

typedef struct thread {
    // user_tid is index of 'arr' where thread is stored
    thread_t user_tid;

    // kernel_tid is the one obtained from clone
    thread_t kernel_tid;

    // stack which thread will use when in execution
    void *stack;
    int stack_size;

    // function which is to be executed by the thread
    void *(*funcptr)(void *);

    // arguments to be passed to the function
    void *arg;

    // return value of the function
    void *retval;

    // If equal to 1 means someone already call join on this thread
    char waiting_to_join ;

    // buffer for storing setjmp(), longjmp() info
    jmp_buf setjump_buf;

    int futex;

    void *tls;

    char state;
}thread;

// Maximum number of simultaneous threads
#define NTHREAD 100
#define STACK_SIZE (4096 * 16)
thread* arr[NTHREAD] = {0};
int allocated_count = 0;
int all_ended = 1; // all_ended means all threads have ended

int thread_begin(void* arg) {
    thread_spin_lock(&glock);
    thread* tptr = (thread*) arg;

    int ret = setjmp(tptr->setjump_buf);
    if (ret == 0) {

        // call the function which is to be executed on the thread
        // store the return value
        thread_spin_unlock(&glock);
        void *retval = tptr->funcptr(tptr->arg);
        thread_spin_lock(&glock);
        tptr->retval = retval;
    }
    
    if (tptr->waiting_to_join == 0) {
        tptr->state = ENDED;
        allocated_count--;
    }
    if (allocated_count == 0) {
        all_ended = 1;
        futex(&all_ended, FUTEX_WAKE, 1); // wake up the main() thread who called thread_exit()
    }
    thread_spin_unlock(&glock);
    return 0;
}

void thread_exit(void *retval) {
    pid_t called_tid = gettid();
    int i;
    thread_spin_lock(&glock);
    for (i = 0; i < NTHREAD; i++)
        if (arr[i] && arr[i]->kernel_tid == called_tid)
            break;
    
    // if no thread has been created yet and main() has called thread_exit
    if (i == NTHREAD) {
        thread_spin_unlock(&glock);
        return;
    }
    
    arr[i]->retval = retval;

    if (i == 0) { // thread_exit() has been called from main()
        arr[0]->state = ENDED;
        allocated_count--;
        if (allocated_count == 0)
            all_ended = 1;
        thread_spin_unlock(&glock);

        // wait until all threads finish
        futex(&all_ended, FUTEX_WAIT, 0);
        exit(0);
    } else {
        longjmp(arr[i]->setjump_buf, 1); // longjumping while holding the lock
    }
}

int thread_create(thread_t *thread_id, thread_attr_t *attr, void *(*funcptr) (void *), void *arg) {
    if (thread_id == NULL || funcptr == NULL)
        return EINVAL;
    
    // if the main thread is not stored in array
    if (arr[0] == NULL) {
        thread_spin_init(&glock);
        thread_spin_lock(&glock);
        arr[0] = (thread*) calloc(1, sizeof(thread));
        if (!arr[0]){
            thread_spin_unlock(&glock);
            return errno;
        }
        arr[0]->user_tid = 0;
        arr[0]->kernel_tid = gettid();
        arr[0]->state = RUNNING;
        allocated_count++;
        all_ended = 0;
    }
    else{
        thread_spin_lock(&glock);
    }

    if (allocated_count == NTHREAD) {
        thread_spin_unlock(&glock);
        return EAGAIN; // Resource(arr) temporarily unavailable
    }
    thread *t = (thread*) malloc(sizeof(thread));
    if (!t){
        thread_spin_unlock(&glock);
        return errno;
    }
    
    if( !attr )
        t->stack_size = STACK_SIZE;
    else
        t->stack_size = attr->stack_size ;
    
    if( attr && attr->stack )
        t->stack = attr->stack ;
    else
        t->stack = mmap(NULL, t->stack_size, PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK, -1, 0);

    if( attr && !(attr->stack) )
        attr->stack = t->stack ;

    if (t->stack == MAP_FAILED) {
        free(t);
        thread_spin_unlock(&glock);
        return errno;
    }

    // find a NULL or has_ended entry in arr to store thread
    int i;
    for (i = 0; i < NTHREAD; i++)
        if (arr[i] == NULL || arr[i]->state == ENDED)
            break;
    
    // if arr[i] is not NULL, means that arr[i]->has_ended is true
    if (arr[i]) {
        // cleanup
        munmap(arr[i]->stack, arr[i]->stack_size);
        //free(arr[i]->tls);
        free(arr[i]);
        arr[i] = NULL;
    }
    
    arr[i] = t;
    allocated_count++;
    *thread_id = i;

    t->user_tid = i;
    t->funcptr = funcptr;
    t->arg = arg;
    t->retval = NULL;
    t->waiting_to_join = 0 ;
    //t->tls = calloc(1, 1024 * 4);
    t->state = RUNNING;
    
    t->kernel_tid = clone(thread_begin, t->stack + t->stack_size, 
                                CLONE_VM | CLONE_FS | CLONE_FILES | CLONE_SIGHAND |CLONE_THREAD | CLONE_SYSVSEM |
                                CLONE_PARENT_SETTID | CLONE_CHILD_CLEARTID, t, &t->futex, NULL, &t->futex);
    // if clone() fails
    if (t->kernel_tid == -1) {
        munmap(t->stack, t->stack_size);
        //free(t->tls);
        free(t);
        arr[i] = NULL;
        allocated_count--;
        thread_spin_unlock(&glock);
        return errno;
    }
    thread_spin_unlock(&glock);
    return 0;
}

int thread_join(thread_t thread_id, void **retval){
    // If thread_id is not is found
    if( thread_id >= NTHREAD || thread_id < 0 ){
        return ESRCH ; // No thread with this id
    }
    /* If thread is already over or thread id is invalid
     * Or someone already call join on thread
     */
    thread_spin_lock(&glock);
    if( !arr[thread_id] || arr[thread_id]->waiting_to_join == 1 || arr[thread_id]->state == ENDED) {
        thread_spin_unlock(&glock);
        return ESRCH ; 
    }

    // Set value for waiting_to_join
    arr[thread_id]->waiting_to_join = 1 ;
    int ret ;
    thread_spin_unlock(&glock); // unlock before blocking!
    ret = futex( &(arr[thread_id]->futex), FUTEX_WAIT, arr[thread_id]->kernel_tid );
    if( ret == -1 )
        return errno ;
    
    thread_spin_lock(&glock);
    // Store return value
    if (retval)
        *retval = arr[thread_id]->retval ;
    arr[thread_id]->state = ENDED;
    allocated_count--;
    thread_spin_unlock(&glock);
    return 0 ;
}

int thread_kill(thread_t thread_id, int sig) {
    
    if (thread_id >= NTHREAD || thread_id < 0)
        return ESRCH; // No thread with this id
    
    if (sig == 0)
        return EINVAL;
    thread_spin_lock(&glock);
    if (!arr[thread_id] || arr[thread_id]->state == ENDED) {
        thread_spin_unlock(&glock);
        return ESRCH;
    }
    
    if (arr[0] == NULL) { // thread group leader not present
        thread_spin_unlock(&glock);
        return EINVAL;
    }
    
    int ret = tgkill(arr[0]->kernel_tid, arr[thread_id]->kernel_tid, sig);
    if (ret == -1) {
        thread_spin_unlock(&glock);
        return errno;
    }
    thread_spin_unlock(&glock);
    return 0;
}

int thread_attr_init( thread_attr_t *attr ){
    if( attr == NULL )
        return EINVAL;
    attr->stack_size = STACK_SIZE ;
    attr->stack = NULL ;
    return 0;
}

int thread_attr_destroy( thread_attr_t *attr ){
    if( !attr )
        return EINVAL ;
    if( attr->stack )
        free( attr->stack );
    return 0 ;
}