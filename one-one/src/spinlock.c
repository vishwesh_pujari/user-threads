#include "spinlock.h"
#include<stdio.h>

int volatile atomic_cmp_exchg( thread_spinlock_t *lock, int expected, int desired ){
    return atomic_compare_exchange_strong( lock, &expected, desired );
}

int thread_spin_init( thread_spinlock_t *lock ){
    assert(lock);
    atomic_store( lock, 0 );
    return 0;
}

int thread_spin_lock( thread_spinlock_t *lock ){
    if( !lock )
        return EINVAL ;
    // the value to store in the atomic object if it is as expected
    while( !atomic_cmp_exchg( lock, UNLOCKED, LOCKED) );
    return LOCKED;
}

int thread_spin_trylock( thread_spinlock_t *lock ){
    if( !lock )
        return EINVAL ;
    if( !atomic_cmp_exchg( lock, UNLOCKED, LOCKED) )
        return LOCKED;
    return UNLOCKED;
}

int thread_spin_unlock( thread_spinlock_t *lock ) {
    if( !lock )
        return EINVAL ;
    atomic_cmp_exchg( lock, LOCKED, UNLOCKED);
    return UNLOCKED;
}
