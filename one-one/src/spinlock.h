#include<stdatomic.h>
#include<errno.h>
#include<assert.h>

#define UNLOCKED 0
#define LOCKED 1

typedef volatile int thread_spinlock_t;

int thread_spin_init( thread_spinlock_t *lock );
int thread_spin_lock( thread_spinlock_t *lock );
int thread_spin_trylock( thread_spinlock_t *lock );
int thread_spin_unlock( thread_spinlock_t *lock );
int volatile atomic_cmp_exchg( thread_spinlock_t *lock, int expected, int desired );