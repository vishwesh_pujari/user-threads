#include"thread.h"
#include<ucontext.h>
#include<signal.h>
#include<setjmp.h>
#include<errno.h>
#include<stdio.h>
#include<sys/time.h>
#include<unistd.h>
#include<sys/types.h>

#define KERNEL_THREADS 5
#define USER_THREADS 15
#define SECONDS 0
#define MICROSECONDS 100

/*typedef struct kernel_thread {
    // number of user threads waiting on this kernel thread
    int nwaiting;

    // currently running user thread
    int current;

    int kernel_tid;

    thread arr[USER_THREADS_PER_KERNEL_THREAD];
}kernel_thread;*/

enum state {
    WAITING,
    RUNNING,
    RUNNABLE,
    ENDED
};

typedef struct thread {
    // user_tid is index of 'arr' where thread is stored
    thread_t user_tid;

    // index in the kthreads array
    thread_t kernel_tid;

    // context to be used by makecontext(), swapcontext()
    ucontext_t context;

    // function which is to be executed by the thread
    void *(*funcptr)(void *);

    // arguments to be passed to the function
    void *arg;

    // return value of the function
    void *retval;

    // If equal to 1 means someone already call join on this thread
    char waiting_to_join ;

    // buffer for storing setjmp(), longjmp() info
    jmp_buf setjump_buf;

    char state;

    // which thread has called join on this thread
    int whoiswaiting;

    // the set for pending signals
    sigset_t set;
}thread;

thread *kthreads[KERNEL_THREADS] = {};
int nkthreads = 0; // number of kernel threads
thread *uthreads[USER_THREADS] = {};
int nuthreads = 0; // number of user threads
struct itimerval timer = {{SECONDS, MICROSECONDS}, {SECONDS, MICROSECONDS}};

int thread_create(thread_t *thread_id, thread_attr_t *attr, void *(*funcptr) (void *), void *arg) {
    if (nuthreads == USER_THREADS) {
        return EAGAIN; // Resource(arr) temporarily unavailable
    }

    if (thread_id == NULL || funcptr == NULL) {
        return EINVAL;
    }

    // if the main thread is not stored in array
    if (uthreads[0] == NULL) {
        uthreads[0] = (thread*) calloc(1, sizeof(thread));
        if (!uthreads[0]) {
            return errno;
        }
        uthreads[0]->user_tid = 0;
        uthreads[0]->kernel_tid = getpid();
        uthreads[0]->state = RUNNABLE;
        uthreads[0]->whoiswaiting = -1;
        uthreads[0]->waiting_to_join = 0;
        if (sigemptyset(&arr[0]->set) == -1) {
            interrupt_enable();
            return errno;
        }
        allocated_count++;
        signal(SIGVTALRM, sighandler);
    }
}